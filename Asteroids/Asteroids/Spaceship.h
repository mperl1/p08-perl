//
//  Spaceship.h
//  Asteroids
//
//  Created by Matt Perl on 5/9/17.
//  Copyright © 2017 Matt Perl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Spaceship : UIImageView

@property (nonatomic) int hp;

@end

//
//  GameView.m
//  Asteroids
//
//  Created by Matt Perl on 5/9/17.
//  Copyright © 2017 Matt Perl. All rights reserved.
//

#import "GameView.h"

@implementation GameView

- (id) initWithCoder:(NSCoder *)aDecoder{
    
    self = [super initWithCoder:aDecoder];
    
    if(self){
        _currentScore = 0;
        _highScore = 0;
        
        _laserArr = [[NSMutableArray alloc] init];
        _bigAsteroidArr = [[NSMutableArray alloc] init];
        _smallAsteroidArr = [[NSMutableArray alloc] init];
        
        _ship = [[Spaceship alloc] initWithImage:[UIImage imageNamed:@"ship"]];
        [_ship setHp: 3];
        CGRect shipFrame = _ship.frame;
        shipFrame.origin.x = 170;
        shipFrame.origin.y = 585;
        shipFrame.size.width = 74;
        shipFrame.size.height = 59;
        [_ship setFrame:shipFrame];
        
        _ammo = 5;
        [self populateLaserArr];
        
        [self populateAsteroidArrs];
        
        [self addSubview:_ship];
    }
    
    return self;
}

- (IBAction) moveLeftButton:(id) sender{
    //NSLog(@"%f", _ship.frame.origin.x);
    //NSLog(@"%f", _ship.frame.origin.y);
    
    CGPoint p = _ship.center;
    
    if(p.x-40 < 0){
        p.x = 37;// + _ship.frame.size.width;
    }
    else{
        p.x -= 20;
    }
    
    [_ship setCenter: p];
    
    //NSLog(@"%f", _ship.frame.origin.x);
    //NSLog(@"%f", _ship.frame.origin.y);
}

- (IBAction) moveRightButton:(id) sender{
    
    CGRect bounds = [self bounds];
    CGPoint p = _ship.center;
    
    if(p.x+40 > bounds.size.width){
        p.x = bounds.size.width - 37;
    }
    else{
        p.x += 20;
    }
    
    [_ship setCenter: p];
}

- (void) populateLaserArr{
    
    CGPoint p = _ship.center;
    //NSLog(@"%f, %f", p.x, p.y);
    for(int i = 0; i < _ammo; i++){
        Laser *l = [[Laser alloc] initWithImage:[UIImage imageNamed:@"Projectile"]];
        [l setLive: false];
        [l setCenter:CGPointMake(p.x, p.y-55)];
        [_laserArr addObject:l];
    }
}

- (void) populateAsteroidArrs{
    
    CGPoint p;
    
    for(int i = 0; i < 3; i++){
        BigAsteroid *ba = [[BigAsteroid alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"BA%d",i+1]]];
        [ba setDy:2];
        p = CGPointMake(([self bounds].size.width/(i+1)) - 100, 0);
        [ba setCenter:p];
        [_bigAsteroidArr addObject:ba];
        [self addSubview:ba];
    }
    
    for(int i = 0; i < 2; i++){
        SmallAsteroid *sa = [[SmallAsteroid alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"SA%d", i+1]]];
        [sa setDy:2];
        p = CGPointMake(([self bounds].size.width)/(i+2), 0);
        [sa setCenter:p];
        [_smallAsteroidArr addObject:sa];
        [self addSubview:sa];
    }
}

- (IBAction) shootButton:(id) sender{
    
    if(_ammo > 0){
        CGPoint p = _ship.center;
        for(int i = 0; i < [_laserArr count]; i++){
            Laser *tmpLaser = [_laserArr objectAtIndex:i];
            if([tmpLaser live] == false){
                [tmpLaser setLive:true];
                [tmpLaser setDy:30];
                [tmpLaser setCenter:CGPointMake(p.x, p.y-55)];
                [self addSubview:tmpLaser];
                _ammo--;
                [_ammoTextField setText:[NSString stringWithFormat:@"%d", _ammo]];
                break;
            }
        }
    }
}

- (void) updateCurrentScore{
    [_currentScoreTextField setText:[NSString stringWithFormat:@"%d", _currentScore]];
}

- (void) updateHighScore{
    
    if(_currentScore > _highScore){
        [_highScoreTextField setText:[NSString stringWithFormat:@"%d", _currentScore]];
    }
    [_currentScoreTextField setText:[NSString stringWithFormat:@"%d", 0]];
}

-(void)arrange:(CADisplayLink *)sender{
    
    //NSLog(@"In arrange");
    for(int i = 0; i < [_laserArr count]; i++){
        Laser *tmpLaser = [_laserArr objectAtIndex:i];
        
        if([tmpLaser live] == true){
            CGPoint p = [tmpLaser center];
            p.y -= [tmpLaser dy];
            [tmpLaser setCenter:p];
        }
    }
    
    //check if laser reached top of screen or collided w/ asteroids
    for(int i = 0; i < [_laserArr count]; i++){
        Laser *tmpLaser = [_laserArr objectAtIndex:i];
        
        if([tmpLaser live] == true){
            
            if([tmpLaser center].y == 0){
                [tmpLaser setLive: false];
                [tmpLaser removeFromSuperview];
                _ammo++;
                [_ammoTextField setText:[NSString stringWithFormat:@"%d", _ammo]];
            }
            else{
                for(int j = 0; j < [_bigAsteroidArr count]; j++){
                    BigAsteroid *ba = [_bigAsteroidArr objectAtIndex:j];
                    if(CGRectContainsPoint([ba frame], [tmpLaser center])){
                        CGPoint p = [ba center];
                        p.y = 0;
                        [ba setCenter:p];
                    
                        [tmpLaser setLive:false];
                        [tmpLaser removeFromSuperview];
                        _ammo++;
                        [_ammoTextField setText:[NSString stringWithFormat:@"%d", _ammo]];
                        _currentScore += 1000;
                        [self updateCurrentScore];
                        break;
                    }
                }
            
                for(int j = 0; j < [_smallAsteroidArr count]; j++){
                SmallAsteroid *sa = [_smallAsteroidArr objectAtIndex:j];
                
                    if(CGRectContainsPoint([sa frame], [tmpLaser center])){
                        CGPoint p = [sa center];
                        p.y = 0;
                        [sa setCenter:p];
                    
                        [tmpLaser setLive:false];
                        [tmpLaser removeFromSuperview];
                        _ammo++;
                        [_ammoTextField setText:[NSString stringWithFormat:@"%d", _ammo]];
                        _currentScore += 500;
                        [self updateCurrentScore];
                        break;
                    }
                }
            }
        }
    }
    
    for(int i = 0; i < [_bigAsteroidArr count]; i++){
        BigAsteroid *ba = [_bigAsteroidArr objectAtIndex:i];
        
        CGPoint p = [ba center];
        p.y += [ba dy];
        [ba setCenter:p];
    }
    
    for(int i = 0; i < [_smallAsteroidArr count]; i++){
        SmallAsteroid *sa = [_smallAsteroidArr objectAtIndex:i];
        
        CGPoint p = [sa center];
        p.y += [sa dy];
        [sa setCenter:p];
    }
     
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

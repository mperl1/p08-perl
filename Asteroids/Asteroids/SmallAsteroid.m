//
//  SmallAsteroid.m
//  Asteroids
//
//  Created by Matt Perl on 5/9/17.
//  Copyright © 2017 Matt Perl. All rights reserved.
//

#import "SmallAsteroid.h"

@implementation SmallAsteroid

@synthesize dx, dy;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

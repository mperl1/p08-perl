//
//  GameView.h
//  Asteroids
//
//  Created by Matt Perl on 5/9/17.
//  Copyright © 2017 Matt Perl. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Spaceship.h"
#import "BigAsteroid.h"
#import "SmallAsteroid.h"
#import "Laser.h"

@interface GameView : UIView{
    
}

@property (nonatomic, strong) Spaceship *ship;

@property (nonatomic) NSMutableArray *bigAsteroidArr;
@property (nonatomic) NSMutableArray *smallAsteroidArr;
@property (nonatomic) NSMutableArray *laserArr;

@property (nonatomic, strong) IBOutlet UITextField *ammoTextField;
@property (nonatomic, strong) IBOutlet UITextField *currentScoreTextField;
@property (nonatomic, strong) IBOutlet UITextField *highScoreTextField;

@property (nonatomic) int ammo;
@property (nonatomic) int currentScore;
@property (nonatomic) int highScore;

-(void)arrange:(CADisplayLink *)sender;

@end

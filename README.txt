@author Matt Perl

The goal of this project is to make an Asteroids clone.  The main gameplay will consist of a button to fire lasers, and the ability to move based on where you're holding on the screen.  The asteroids will spawn at the top of the screen, and break into smaller asteroids when destroyed.  The ship will have 3 bars of health, and will lose one every time it collides with an asteroid.  Additionally, shield power-ups will periodically spawn, which will give the ship immunity for a brief period of time.

To-do:
- Add ship HP and damage detection
- Fix bug when you can get extra ammo when you hit an asteroid at the top of the screen
- Add logic to handle asteroids hitting bottom of the screen

For the future:
- Small asteroids spawn from bigger asteroids dying
- Asteroids speed increases as time passes
- Spawn shield power-ups
- Allow movement on the y-axis

IMPORTANT NOTE: As of right now, the CADisplayLink is not invoking the specified method, and nothing I've done has fixed it.  Unfortunately, this means that the game has no real playability without the DisplayLink functioning. I have, however, added the core logic of the game for when I figure out what's going on.

--UPDATE: Above bug has been fixed and game is at least functional!

Note: Ideally I would have been able to put much more time into this project, however the last 3-4 weeks have left me with barely enough time to even eat - much less work on this in my spare time.  I hope you understand that it was not from lack of caring, but rather from the workload from the other 3 CS courses that I'm taking.
